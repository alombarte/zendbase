-- Generic database reusable in many sites.

CREATE TABLE `documents` (
	`id` int(11) NOT NULL auto_increment,
	`id_parent` int(11) NOT NULL,
	`lft` int(11) NOT NULL,
	`rgt` int(11) NOT NULL,
	`depth` int(11) NOT NULL default '1',
	`priority` int(11) NOT NULL DEFAULT '1',
	`name` varchar(255) NOT NULL, -- Home page/root has the name 'HOME'
	`title` varchar(255) default NULL,
	`subtitle` varchar(255) default NULL,
	`excerpt` varchar(255) default NULL,
	`button_text` varchar(100) default NULL,
	`body` text,
	`status` enum('draft','public','private','hidden') NOT NULL default 'draft',
	`lang` varchar(10) NOT NULL default 'es_ES',
	`keywords` text COMMENT 'META Keywords',
	`description` text COMMENT 'META Description',
	`layout` varchar(255) NOT NULL default 'default',
	`highlited` smallint(1) NOT NULL default '0' COMMENT 'Document is important? Must be highlited?',
	`show_related` int(1) NOT NULL default '1',
	`modified` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP,
	`creation` timestamp NOT NULL default '0000-00-00 00:00:00',
	PRIMARY KEY  (`id`),
	UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='HTML documents (hierarchy:preorder tree traversal)';

CREATE TABLE `document_links` (
	id integer NOT NULL AUTO_INCREMENT,
	id_doc integer NOT NULL,
	priority int(11) NOT NULL DEFAULT '1',
	title varchar(255),
	description TEXT,
	url varchar(400),
	lang varchar(10) NOT NULL default 'es_ES',
	button_text varchar(50),
	FOREIGN KEY (`id_doc`) REFERENCES documents(id) ON DELETE CASCADE,
	PRIMARY KEY (`id`)
)ENGINE= InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `images` (
	id integer NOT NULL AUTO_INCREMENT,
	alt varchar(255),
	src varchar(255) NOT NULL,
	class varchar(50),
	PRIMARY KEY (`id`)
)ENGINE= InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `images_in_docs` (
	ref_doc integer NOT NULL,
	ref_image integer NOT NULL,
	FOREIGN KEY (`ref_doc`) REFERENCES documents(id) ON DELETE CASCADE,
	FOREIGN KEY (`ref_image`) REFERENCES images(id) ON DELETE CASCADE,
	PRIMARY KEY (`ref_doc`, `ref_image`)
)ENGINE= InnoDB DEFAULT CHARSET=utf8;


--
-- Insert basic data
--

INSERT INTO `documents` VALUES (1, 0, 1, 8, 0, 0, 'HOME', 'Welcome to my running app in Zendbase', 'Start coding now', 'This is my site', 'Click here', 'This document is not linked anywhere, but it can be accessed by typing in the URL ''/HOME''', 'public', 'es_ES', 'felis risus, porttitor nec, pharetra id, dapibus ut, mauris.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus nunc. Nam tincidunt.', 'default', 0, 1, '2008-10-10 17:10:02', '2008-09-24 17:17:58');
INSERT INTO `documents` VALUES (2, 1, 2, 5, 1, 0, 'doc-1', 'Document 1', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec mauris nunc, rhoncus quis, congue in, egestas nec, est. Integer ligula felis, gravida sit amet, vestibulum sed, aliquam eget, ligula. Ut aliquam tincidunt magna. Nam velit nulla, mollis ac, h', 'Read more!', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus nunc. Nam tincidunt. Maecenas et urna sit amet tellus varius fringilla. Proin sagittis lectus ut nunc hendrerit varius. Sed semper, est sed posuere commodo, orci mauris malesuada turpis, ac laoreet sapien lacus sed enim. In malesuada, erat ut tempor elementum, lectus enim eleifend metus, in sodales libero est ac turpis. Vestibulum eget sem ac dui commodo accumsan. Maecenas laoreet molestie nulla. Nam felis risus, porttitor nec, pharetra id, dapibus ut, mauris. Vestibulum sollicitudin nisi sed mauris. Donec vestibulum velit. Donec convallis metus at augue. Donec aliquam mi ac augue. Donec magna tellus, euismod at, volutpat a, ullamcorper in, elit. </p><p>Pellentesque est nisl, pellentesque ut, aliquam in, fringilla non, risus. Donec diam odio, egestas ac, adipiscing eget, posuere vitae, magna. Mauris vitae dolor. Donec sagittis est eu turpis. Curabitur aliquet, mi ac commodo elementum, metus ante ullamcorper nisl, et faucibus libero risus vel velit. Maecenas gravida pretium purus. Nullam sed erat ac nisi consectetuer egestas. Praesent molestie hendrerit arcu. Cras semper augue vel sapien. Duis vel risus. Integer sit amet pede ut enim iaculis consequat.</p>', 'public', 'es_ES', 'sapien, iaculis, consequat', 'Pellentesque est nisl, pellentesque ut, aliquam in, fringilla non, risus. Donec diam odio, egestas ac, adipiscing eget, posuere vitae, magna. ', 'default', 0, 1, '2008-10-10 17:11:24', '2008-09-24 17:17:58');
INSERT INTO `documents` VALUES (12, 1, 6, 7, 1, 0, 'doc-2', 'Consectetuer adipiscing elit', 'Pellentesque est nisl, pellentesque ut, aliquam in, fringilla non, risus.', 'Integer sit amet pede ut enim iaculis consequat. ', NULL, 'Pellentesque est nisl, pellentesque ut, aliquam in, fringilla non, risus. Donec diam odio, egestas ac, adipiscing eget, posuere vitae, magna. Mauris vitae dolor. Donec sagittis est eu turpis. Curabitur aliquet, mi ac commodo elementum, metus ante ullamcorper nisl, et faucibus libero risus vel velit. Maecenas gravida pretium purus. Nullam sed erat ac nisi consectetuer egestas. Praesent molestie hendrerit arcu. Cras semper augue vel sapien. Duis vel risus. Integer sit amet pede ut enim iaculis consequat. ', 'public', 'es_ES', NULL, NULL, 'default', 0, 1, '2008-10-10 17:12:05', '2008-10-06 22:38:54');
INSERT INTO `documents` VALUES (13, 2, 3, 4, 2, 0, 'a-son-of-a-good-looking-document', 'Some text', 'Good looking title', 'Wow!!', 'Read more!', 'Pellentesque est nisl, pellentesque ut, aliquam in, fringilla non, risus. Donec diam odio, egestas ac, adipiscing eget, posuere vitae, magna. Mauris vitae dolor. Donec sagittis est eu turpis. Curabitur aliquet, mi ac commodo elementum, metus ante ullamcorper nisl, et faucibus libero risus vel velit. Maecenas gravida pretium purus. Nullam sed erat ac nisi consectetuer egestas. Praesent molestie hendrerit arcu. Cras semper augue vel sapien. Duis vel risus. Integer sit amet pede ut enim iaculis consequat. ', 'public', 'es_ES', NULL, NULL, 'default', 0, 1, '2008-10-10 17:12:44', '2008-10-08 20:27:44');


