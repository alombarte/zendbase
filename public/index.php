<?php
/**
 * Point of entry.
 * 
 * All PHP requests should be directed to this file via .htaccess or Apache using mod_rewirte or mod_alias.
 */

// Get the path to the root folder. ( 2 folders up from the current file):
$root = dirname(dirname(__FILE__));

// If you have access to your php.ini, move the include_path there and remove this line:
set_include_path( $root . '/libs' . PATH_SEPARATOR . get_include_path() );

require $root . '/application/Bootstrap.php';

Bootstrap::execute( $root );