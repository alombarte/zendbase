<?php

require_once 'Zend/Controller/Action.php';

class Zendbase_Controller extends Zend_Controller_Action
{

	/**
	 * Loads data from a given model.
	 *
	 * @param string $model The Model you want to load
	 * @param boolean $return_object Whether you want to return a 'new' object or not. Set to false for static or singleton usage.
	 */
	public function getModel( $model_class, $return_object = true )
	{
		$models_dir = Bootstrap::$application . '/models';
		Zend_Loader::loadClass($model_class, $models_dir);

		if ( $return_object )
		{
			return new $model_class;
		}
	}

}

class Exception_404 extends Zend_Controller_Action_Exception {}
class Exception_500 extends Zend_Controller_Action_Exception {}