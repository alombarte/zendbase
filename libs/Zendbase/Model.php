<?php
abstract class Zendbase_Model
{

	/**
	 * Default database profile. This profile will be used in all Models if no profile is chosen.
	 *
	 * @var string
	 */
	public $db_profile = 'default';

	/**
	 * Stores the parameters passed by the controller. Not intended for internal class usage.
	 *
	 * Overrides any existing param.
	 *
	 * @param array $params Parameters to store.
	 */
	public function setParams( $params )
	{
		$this->params = $params;
	}

	/**
	 * Gets the parameters sent by controller.
	 *
	 * @return array
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * Change the default profile. Each profile is a section in the database.ini configuration file.
	 *
	 * @param string $profile Profile name (section in INI file).
	 */
	public function setProfile( $profile )
	{
		$this->db_profile = $profile;
	}


	/**
	 * Returns the current database profile used in every connection.
	 *
	 * @return string
	 */
	public function getProfile()
	{
		return $this->db_profile;
	}


	/**
	 * Opens a connection to the database if the host is enabled and stores it in the registry.
	 *
	 * @param string $profile Database profile used in the connection.
	 * @return object Database connection or false if DB access is disabled.
	 */
	public function connect( $profile = null )
	{
		if ( $profile == null )
		{
			$profile = $this->getProfile();
		}

		if ( Zend_Registry::isRegistered( "db_$profile" ) )
		{
			return Zend_Registry::get( "db_$profile" );
		}

		$config = new Zend_Config_Ini( Bootstrap::$root.'/config/database.ini', $profile );

		if ( '1' == $config->database->enabled )
		{
			$db = Zend_Db::factory( $config->database );
			$db->getConnection();
			// Force UTF-8:
			$db->query( 'SET NAMES ' . $config->database->setnames );
			/*Zend_Db_Table_Abstract::setDefaultAdapter( $db );*/
		}
		else
		{
			trigger_error( "The database with profile '$profile' has been disabled in the configuration file" );
			$db = false;
		}

		// Store 'db' in the registry so it can be reused later:
		Zend_Registry::set( "db_$profile", $db );

		return $db;
	}


}