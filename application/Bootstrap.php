<?php
/**
 * Class Bootstrap
 */
require_once 'Zend/Loader.php';

class Bootstrap
{
	/**
	 * Root path.
	 *
	 * @var string
	 */
	public static $root;

	/**
	 * Application path.
	 *
	 * @var string
	 */
	public static $application;

	/**
	 * Enviroment of this code. Options available depend on the sections defined in config/routes.ini.
	 *
	 * You can use the section, for example, to apply different routing depending on host or language.
	 *
	 * @var string
	 */
	public static $host = 'any_host';

	/**
	 * Starts the execution. Root path is passed to avoid recalculation.
	 *
	 * @param string $root Path to root.
	 */
	public static function execute( $root )
	{
		// Set paths:
		self::$root = $root;
		self::$application = dirname( __FILE__ );
		self::overwritePhpIni();
		self::startMVC();
	}

	/**
	 * Overwrite several values of php.ini file when you can't change them there.
	 *
	 * If you have access, it's better to remove that function and set the values in the php.ini file.
	 */
	public static function overwritePhpIni()
	{
		// Show errors on the screen. Disable in production:
		ini_set( 'display_errors', true );

		// Log errors to 'logs' folder:
		ini_set( 'log_errors', 'On' );
		ini_set( 'error_log', self::$root . '/logs/errors.log' );

		// Allow short tags <? (instead of <?php) for more flexible view scripts.
		ini_set( 'short_open_tag', '1' );

		// Uncomment the following line to test error reporting:
		// 	trigger_error( 'Testing the error logging' );

		// Show all errors, notices, warnings...
		error_reporting( E_ALL|E_STRICT );

		// Set Timezone as required by php 5.1+
		date_default_timezone_set('Europe/Madrid');
	}

	/**
	 * Sets the controller and view properties and executes the front controller, sending the output buffer.
	 */
	public static function startMVC()
	{
		// Controller setup:
		Zend_Loader::registerAutoload();
		$controller_front = Zend_Controller_Front::getInstance();
		$controller_front->throwExceptions( false );
		$controller_front->returnResponse( true );
		$controller_front->setControllerDirectory(
		self::$application . '/controllers'
		);

		// Any requested URL not mapped in the router is sent to IndexController:
//		$controller_front->setParam('useDefaultControllerAlways', true);

		// Set routing defined in routes.ini file, depending on which enviroment are you:
		$router = $controller_front->getRouter();
		// Remove any default routes
		$router->removeDefaultRoutes();


		$config = new Zend_Config_Ini( self::$root.'/config/routes.ini', self::$host );
		$route_mapper = new Zend_Controller_Router_Rewrite();
		$route_mapper->addConfig( $config );
		$controller_front->setRouter( $route_mapper );


		$plugin = new Zend_Controller_Plugin_ErrorHandler();
		$controller_front->registerPlugin($plugin);

		// View setup:
		$view = new Zend_View();

		/*
		 If you'd like to work with Smarty use:
		 $view = new Zendbase_Smarty();
		 And comment the setEncoding below.
		 You'll need to download Smarty inside libs and create the compile directories as well, ensure they are writeable by server.
		 */

		$view->setScriptPath( self::$application . '/views' );
		$view->setEncoding( 'UTF-8' );
		$view->setHelperPath( self::$application . '/views/helpers' );
		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer( $view );
		Zend_Controller_Action_HelperBroker::addHelper( $viewRenderer );

		$layout_options = array(
			'layout'     => 'home',
			'layoutPath' => self::$application . '/views/layout',
			'contentKey' => 'CONTENT',           // ignored when MVC not used
		);

		// Use Zend Layout:
		$layout = Zend_Layout::startMvc( $layout_options );

		$response = $controller_front->dispatch();
		$response->setHeader( 'Content-Type', 'text/html; charset=UTF-8', true );
		$response->sendResponse();

	}

}