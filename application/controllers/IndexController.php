<?php

/**
 * This is the default controller for the home page.
 */
class IndexController extends Zendbase_Controller
{

	/**
	 * Instead of getting the BODY from database, we use the script in index/index.phtml and we take only the keywords from DB.
	 */
	public function indexAction()
	{
		// Load the class Document_Index:
		$document_model = $this->getModel( 'Document_Index' );

		// Pick the row named 'HOME':
		$page = $document_model->getDocument( 'HOME' );

		$this->view->page = $page;
	}
}
