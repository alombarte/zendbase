<?php

/**
 * This is the default controller for the home page.
 */
class DocumentController extends Zendbase_Controller
{
	public function indexAction()
	{
		$document_name = $this->_getParam( 'name' );
		if ( null === $document_name )
		{
			throw new Exception_404( 'The param "name" has not been mapped by the Router. You need a line in the routes.ini like "anyurl.map.name = 1"' );
		}

		$document_model = $this->getModel( 'Document_Index' );
		$document = $document_model->getDocument( $document_name );

		if ( empty( $document ) )
		{
			throw new Exception_404( "Document '$document_name' not found in database" );
		}

		$path = $document_model->getPath( $document_name );

		// This field could also define the levels of depth.
		if ( $document['show_related'] )
		{
			// Get the next level of children:
			$children = $document_model->getChildren( $document_name, 1 );
			$this->view->children = $children;
		}

		// The assign 'page' is used in HEAD as well:
		$this->view->page = $document;

		$this->view->path = $path;

		$this->_helper->layout()->setLayout( $document['layout'] );

	}

	/**
	 * Add a new children in the database:
	 */
	public function addAction()
	{
		$document_name = $this->_getParam( 'name' );
		$document_model = $this->getModel( 'Document_Index' );
		$document = $document_model->getDocument( $document_name );

		if ( empty( $document ) )
		{
			throw new Exception_404( "Document '$document_name' not found in database" );
		}

		$form = $this->getAddDocumentForm();

		if ( $this->getRequest()->isPost() )
		{
			if ( $form->isValid( $_POST ) )
			{
				$params['parent_name'] 	= $document_name;
				$params['name'] 	= $form->getValue( 'name' );
				$params['title'] 	= $form->getValue( 'title' );
				$params['subtitle'] = $form->getValue( 'subtitle' );
				$params['excerpt'] 	= $form->getValue( 'excerpt' );

				$document_model->addChildren( $params );
				$this->view->added_doc = $params['name'];
			}
		}

		$this->view->form = $form;
		//id_parent 	lft 	rgt 	name 	title 	subtitle 	excerpt 	button_text 	body 	status 	lang 	keywords META Keywords	description META Description	layout 	highlited Document is important? Must be highlited?	show_related 	modified 	creation
		//
	}
	/**
	 * Fixes a completelly messed up tree of docy.
	 *
	 */
	public function rebuildTreeAction()
	{
		// Rebuild the tree:
		$document_model = $this->getModel( 'Document_Index' )->rebuildTree( 0,0 );
	}

	protected function getAddDocumentForm()
	{
		$form = new Zend_Form(array(
			'method'   => 'post',
			'elements' => array(
				'name' => array('text', array(
				'required' => true,
				'label'=>'URI',
				'class' => 'm'
			)),
				'title' => array('text', array(
				'required' => true,
				'label'=>'Título',
				'class' => 'm'
			)),
				'subtitle' => array('text', array(
				'required' => false,
				'label'=>'Subítulo',
				'class' => 'm'
			)),
				'excerpt' => array('textarea', array(
				'required' => false,
				'label' => 'Resumen:' ,
				'class' => 'xs'
			)),
				'submit' => array('submit', array(
				'label' => 'Enviar',
				'class' => 's'
			))
			),
		));

		return $form;
	}


}
