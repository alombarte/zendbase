<?php

class ContactController extends Zend_Controller_Action
{

	/**
	 * A sample for setting a FORM.
	 *
	 */
	public function indexAction()
	{
		$this->view->title = 'Contact information';
		//$this->_helper->layout()->setLayout('alternative');
		$form = $this->getCommentForm();

		if ($this->getRequest()->isPost()) {
			if ($form->isValid($_POST)) {
				$comment = $form->getValue('comment');
				$this->view->comment = $comment;
			}
		}

		$this->view->form = $form;
	}

	/**
	 * This function returns a simple form for adding a comment
	 */
	public function getCommentForm()
	{
		$form = new Zend_Form(array(
						'method'   => 'post',
						'elements' => array(
								'comment' => array('textarea', array(
										'required' => true,
										'label' => 'Comment:' ,
					'class' => 'm'

					)),
					'name' => array('text', array(
					'required' => true,
					'label'=>'Name',
					'class' => 'm'
					)),
								'submit' => array('submit', array(
									 'label' => 'Send',
					'class' => 's'
					))
					),
					));

					return $form;
	}
}