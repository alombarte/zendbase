<?php

/**
 * This is the default exception handler
 */
class ErrorController extends Zendbase_Controller
{
	public function errorAction()
	{


		$errors = $this->_getParam( 'error_handler' );
		$exception_class = get_class( $errors->exception );

		switch ( $exception_class )
		{
			case 'Exception_404':
			case 'Zend_Controller_Dispatcher_Exception':
			case 'Zend_Controller_Dispatcher_Exception':
			case 'Zend_Controller_Action_Exception':
				$response_code = 404;
				break;
			default:
				$response_code = 500;
				break;
		}

		$this->getResponse()->setHttpResponseCode( $response_code );
		$this->getResponse()->clearBody();

		// Search in configuration if exceptions have to be shown.
		$settings = new Zend_Config_Ini( Bootstrap::$root . '/config/settings.ini', 'Exceptions' ) ;
		if ( '1' == $settings->get( 'display_traceback' ) )
		{
			$exception['file'] = $errors->exception->getFile();
			$exception['msg'] = $errors->exception->getMessage();
			$exception['traceback'] = $errors->exception->getTraceAsString();
			$this->view->exception = $exception;
		}

		$this->view->code = $response_code;
	}
}