<?php
class Document_Index extends Zendbase_Model
{

	protected $document_row; // Stores the document row.

	/**
	 * Retrieves the current document.
	 *
	 * @return unknown
	 */
	public function getData()
	{
		$params = $this->getParams();

		/*if ( !isset( $params['name'] ) )
		 {
			throw new Exception( 'Impossible to retrieve documents without a name' );
			}*/

		if ( !isset( $params['action'] ) )
		{
			$action = 'getDocument';
		}
		else
		{
			$action = $params['action'];
		}

		unset( $params['action'] );
		return $this->$action( $params );
	}

	/**
	 * Retrieves a sinle document. This action is used if none specified.
	 *
	 * @param array $doc Document properties ('name' mandatory).
	 */
	public function getDocument( $name )
	{
		$registry_doc_name = 'doc_' . $name;

		if ( Zend_Registry::isRegistered( $registry_doc_name ) )
		{
			return Zend_Registry::get( $registry_doc_name );
		}

		$db = $this->connect();
		$document = $db->fetchRow( 'select * from documents where name=?', $name );
		Zend_Registry::set( 'doc_'.$name, $document );

		return $document;
	}

	/**
	 * Retrieves a sinle document by id.
	 *
	 * @param array $id Document id.
	 */
	public function getDocumentById( $id )
	{
		$registry_doc_name = 'doc_' . $id;

		if ( Zend_Registry::isRegistered( $registry_doc_name ) )
		{
			return Zend_Registry::get( $registry_doc_name );
		}

		$db = $this->connect();
		$document = $db->fetchRow( 'select * from documents where id=?', $id );
		Zend_Registry::set( 'doc_'.$id, $document );
		Zend_Registry::set( 'doc_'.$document['name'], $document );

		return $document;
	}

	/**
	 * Get list of links assigned to the current document.
	 *
	 * @param string $id_or_name
	 * @param integer $limit Limit results to number.
	 * @return array
	 */
	public function getDocumentLinks( $id_or_name, $limit = 100 )
	{
		$db = $this->connect();

		$sql = <<<BYNAME_OR_ID
SELECT
# Get all the document links for a given name
	links.*
FROM
	document_links links
LEFT JOIN documents docs ON links.id_doc = docs.id
WHERE
	docs.name=?
OR
	links.id_doc=?
ORDER BY
	links.priority DESC
LIMIT ?
BYNAME_OR_ID;

		return $db->fetchAll( $sql, array( $id_or_name, $id_or_name, $limit ) );

	}


	/**
	 * Returns all the children for a given node. You can limit the depth of children to get limiting 'depth'
	 *
	 * @param string $root Starting node name.
	 * @param integer $depth Number of levels you want to get.
	 */
	public function getChildren( $name, $depth = 1000 )
	{
		$node = $this->getDocument( $name );
		$current_depth = $node['depth'];

		// Retrieve all descendants of the root node
		$sql = <<<CHILDREN
SELECT
# Get all the children for document
	name,
	lft,
	rgt,
	title,
	excerpt,
	button_text
FROM
	documents
WHERE
	lft BETWEEN ?+1 AND ?-1
AND
	depth <= ?
ORDER BY
	lft ASC
CHILDREN;

		$db = $this->connect();
		$tree = $db->fetchAll( $sql, array( $node['lft'], $node['rgt'], $current_depth + $depth ) );
		$tree['descendants'] = ( $node['rgt'] - $node['lft'] - 1) / 2;

		return $tree;
	}

	/**
	 * Returns the path to a node
	 *
	 * @return string $name Name of the document.
	 */
	public function getPath( $name )
	{
		$node = $this->getDocument( $name );

		// Retrieve all descendants of the root node
		$sql = <<<CHILDREN
SELECT
	title,
	name
FROM
	documents
WHERE
	lft < ? AND rgt > ?
ORDER BY
	lft ASC
CHILDREN;

		$db = $this->connect();
		$path = $db->fetchAll( $sql, array( $node['lft'], $node['rgt'] ) );

		return $path;
	}

	/**
	 * Adds a children for the selected document name and rebuilds the tree.
	 *
	 * @param array $doc Properties of the new document. There must exist 'parent_name' key.
	 */
	public function addChildren( $doc )
	{
		if ( !isset( $doc['parent_name'] ) )
		{
			throw new Exception_500( 'Impossible to add a children without specifying a "parent_name". If you need a ROOT node use the "HOME" parent' );
		}

		$db = $this->connect();
		$current_doc = $this->getDocument( array( 'name' => $doc['parent_name'] ) );
		unset( $doc['parent_name'] );


		$doc['lft'] = 0; // Left and right values are calculated while rebuilding the tree.
		$doc['rgt'] = 0;
		$doc['id_parent'] = $current_doc['id'];
		$doc['depth'] = $current_doc['depth']+1;
		$doc['creation'] = new Zend_Db_Expr( 'NOW()' );
		$db->insert( 'documents', $doc );

		//Rebuild the whole tree:
		$this->rebuildTree( 0,0 );

	}

	/**
	 * Adds a children without rebuilding the tree.
	 *
	 * FIXME: Still not working properly. I'll have a look later....
	 */
	public function addChildrenNoRebuild( $doc = array() )
	{
		$db = $this->connect();

		// When a document name is not given, then the document is added as a children of the ROOT:
		if ( !isset( $doc['parent_name'] ) )
		{
			$sql_lft_rgt = "SELECT max(rgt)+1 lft,max(rgt)+2 rgt FROM documents";
			$next_node = $db->fetchRow( $sql_lft_rgt );
			if ( empty( $next_node ) )
			{
				throw new Exception_500( 'Failed to add a new node when determining next LFT and RGT values' );
			}

			$doc['id_parent'] = 0;
			$doc['lft'] = $next_node['lft'];
			$doc['rgt'] = $next_node['rgt'];
			$doc['depth'] = $current_doc['depth']+1;
			$doc['creation'] = new Zend_Db_Expr( 'NOW()' );

			$db->insert( 'documents', $doc );

			return $db->lastInsertId();
		}
		else
		{
			$db->beginTransaction();
			try
			{
				// Get current document:
				$current_doc = $this->getDocument( array( 'name' => $doc['parent_name' ] ) );
				unset( $doc['parent_name'] );

				// Update right values:
				$data = array(
					'rgt' => new Zend_Db_Expr( 'rgt+2' )
				);

				// Equivalent for: UPDATE documents SET rgt=rgt+2 WHERE rgt>=X
				$db->update( 'documents', $data, 'rgt>=' . $current_doc['rgt'] );
				/*
				 // Update left values:
				 $data = array(
					'lft' => new Zend_Db_Expr( 'lft+2' )
					);

					// Equivalent for: UPDATE documents SET lft=lft+2 WHERE lft>X
					$db->update( 'documents', $data, 'lft>' . $current_doc['rgt'] );
					*/

				// Now, there is room for the node, insert it.
				$doc['lft'] = $current_doc['rgt'];
				$doc['rgt'] = $current_doc['rgt'] + 1;
				$doc['id_parent'] = $current_doc['id'];
				$doc['depth'] = $current_doc['depth']+1;
				$doc['creation'] = new Zend_Db_Expr( 'NOW()' );
				$db->insert( 'documents', $doc );

				$db->commit();

			}
			catch ( Exception $e )
			{
				$db->rollBack();
				throw $e; // The ErrorController knows how to catch this.
			}
		}
	}

	/**
	 * Adds a brother node in its right.
	 *
	 * TODO: Everything.
	 */
	public function addBrother( $name, $lang )
	{
		/*
		 if ( id_parent = 0 ) throw exception('Adding a node brother is not allowed because it must be unique' );

		 $sql[] = "UPDATE documents SET rgt=rgt+2 WHERE rgt>".$doc['rgt'];
		 $sql[] = "UPDATE documents SET lft=lft+2 WHERE lft>".$doc['rgt'];

		 $sql[]="INSERT INTO documents(blah)
		 */
		throw new Exception( 'Method not implemented yet' );
	}

	/**
	 * Regenerates the whole tree with the proper left and right values.
	 *
	 * The elements of the same level are sorted using the priority value. The higher priority, the sooner appears the node in lists.
	 *
	 * To regenerate tree from root call rebuild_tree(0,1)
	 */
	public function rebuildTree( $parent, $left )
	{
		$db = $this->connect();

		// the right value of this node is the left value + 1
		$right = $left+1;

		// get all children of this node. Bigger values of 'priority' are taken into account before.
		$sql = "SELECT id FROM documents WHERE id_parent=$parent ORDER BY priority DESC";
		$results = $db->fetchAll( $sql );
		if ( !empty( $results ) )
		{
			foreach ( $results as $row )
			{
				// recursive execution of this function for each
				// child of this node
				// $right is the current right value, which is
				// incremented by the rebuild_tree function
				$right = $this->rebuildTree( $row['id'], $right );
			}
		}

		// we've got the left value, and now that we've processed
		// the children of this node we also know the right value

		$db->query( 'UPDATE documents SET lft='.$left.', rgt='.
		$right.' WHERE id="'.$parent.'";'
		);

		// return the right value of this node + 1
		return $right+1;
	}

}

?>