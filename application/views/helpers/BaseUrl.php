<?php

/**
 * JUST TESTING: Allows to the views get the Base URL by calling $this->baseUrl() function
 */
class Zend_View_Helper_BaseUrl
{ 
    function baseUrl() 
    { 
	// @test
       	$fc = Zend_Controller_Front::getInstance(); 
		return $fc->getBaseUrl();
    } 
} 
